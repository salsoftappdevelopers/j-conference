import React from 'react'

import MainNavigator from './src/Navigator/MainNavigator';

export default class App extends React.Component {

    render() {
        return <MainNavigator />
    }
}