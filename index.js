/**
 * @format
 */

import {AppRegistry,Platform,UIManager} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
if(Platform.OS === 'android'){
    UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
    AppRegistry.registerHeadlessTask('RNCallKeepBackgroundMessage', () => ({ name, callUUID, handle }) => {
        return Promise.resolve();
      });
}
AppRegistry.registerComponent(appName, () => App);
