import Api from './Api'
// var currentPlugin = "janus.plugin.videocall"
var sessionId = null
var session = null
var handleId = null
var handle = null
const initializeJanusServer = (successCallback, errorCallback) => {
    let dataToSend = {
        "janus": "create",
        "transaction": randomString(12)
    }
    Api.post('', dataToSend,
        success => {
            sessionId = success.data.id
            session = '/' + success.data.id
            successCallback(success)
        }, error => {
            errorCallback(error)
        })
}

const initializeJanusPlugin = (successCallback, errorCallback) => {
    let data = {
        "janus": "attach",
        "plugin": "janus.plugin.videocall",
        "transaction": randomString(12)
    }
    Api.post(session, data, success => {
        handleId = success.data.id
        handle = session + '/' + success.data.id
        successCallback(success)
    }, error => {
        errorCallback(error)
    })
}

const randomString = (len) => {
    charSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz, randomPoz + 1);
    }
    return randomString;
}
const getSession = () => {
    return session
}
const getRegisteredUsers = (successCallback, errorCallback) => {
    Api.post(handle, {
        "janus": "message",
        body: {
            "request": "list",
        },
        "transaction": randomString(12)
    },success=>{
        successCallback(success)
    },error=>{
        errorCallback(errorCallback)
    })
}

const registerUser = (data,successCallback, errorCallback) => {
    Api.post(handle, {
        "janus": "message",
        body: {
            ...data
        },
        "transaction": randomString(12)
    },success=>{
        successCallback(success)
    },error=>{
        errorCallback(error)
    })
}
const acceptCall = (callData,successCallback, errorCallback) => {
    Api.post(handle, {
        "janus": "message",
        body: {
            "request": "accept",
            "username":callData.username
         },
         jsep:callData.jsep,
        "transaction": randomString(12)
    },success=>{
        successCallback(success)
    },error=>{
        errorCallback(error)
    })
}
const callHung = (successCallback, errorCallback) => {
    Api.post(handle, {
        "janus": "message",
        body: {
            "request": "hangup"
         },
        "transaction": randomString(12)
    },success=>{
        successCallback(success)
    },error=>{
        errorCallback(error)
    })
}
const callRegisteredUser = (data,successCallback, errorCallback) => {
    console.log('calling data',data)
    if(data.request != 'call'){
        console.error('Must include request call')
        return
    }
    if(!data.username){
        console.error('Must include registered username')
        return
    }
    if(!data.jsep){
        console.error('Must include webrtcstuff')
        return
    }
    Api.post(handle, {
        "janus": "message",
        body: {
            ...data
        },
        jsep:data.jsep,
        "transaction": randomString(12)
    },success=>{
        successCallback(success)
    },error=>{
        errorCallback(error)
    })
}
// const switchPlugin = (plugin) => {
//     currentPlugin = plugin
//     initializePlugin(plugin)
// }

// const initializePlugin = (plugin) => {

// }

// currentPlugin:currentPlugin,
// switchPlugin:switchPlugin



export default Janus = {
    initializeJanusServer: initializeJanusServer,
    initializeJanusPlugin: initializeJanusPlugin,
    getRegisteredUsers:getRegisteredUsers,
    getSession:getSession,
    randomString: randomString,
    registerUser:registerUser,
    callRegisteredUser:callRegisteredUser,
    callHung:callHung,
    acceptCall:acceptCall
}
