import Janus from './index'
const host = "janus.conf.meetecho.com"
const serverUrl = "https://" + host + "/janus"

const configuration = {
    "iceServers": [
        {
            "url": "stun:stun.l.google.com:19302"
        }
    ]
};
const generalHeaders = {
    "Accept": "application/json",
    "Content-Type": "application/json"
}
const handleServerError = (errorMessage, callback) => {
    console.log(errorMessage)
    callback(errorMessage)
}
const get = (endpoint, success, errorCallback) => {
    let url = serverUrl + endpoint
    let reqConfig = {
        method: 'GET',
        ...generalHeaders,
    }
    fetch(url, reqConfig)
        .then(d => {
            d.json()
                .then(data => {
                    console.log('----------get api hit-----------------------')
                    console.log('get url : ', url)
                    console.log('get res : ', data)
                    console.log('----------end of get api hit-----------------------')
                    success(data)
                })
                .catch(e => {
                    console.log('----------get api hit-----------------------')
                    console.log('get url : ', url)
                    console.log('get parse err : ', e)
                    console.log('----------end of get api hit-----------------------')
                    handleServerError(e, errorCallback)
                })
        })
        .catch(e => {
            console.log('----------get api hit-----------------------')
            console.log('get url : ', url)
            console.log('get err : ', e)
            console.log('----------end of get api hit-----------------------')
            handleServerError(e, errorCallback)
        })
}
const post = (endpoint, data, success, errorCallback) => {
console.log('asd')
    let url = serverUrl + endpoint
    let reqConfig = {
        method: 'POST',
        ...generalHeaders,
        body: JSON.stringify(data)
    }
    fetch(url, reqConfig)
        .then(d => {
            d.json()
                .then(data => {
                    console.log('----------post api hit-----------------------')
                    console.log('post url : ', url)
                    console.log('post data : ', data)
                    console.log('post res : ', d)
                    console.log('----------end of post api hit-----------------------')
                    if (data.janus) {
                        if (data.janus === 'ack') {
                            get(Janus.getSession(), d => {
                                success(d)
                            }, error => {
                                handleServerError(error, errorCallback)
                            })
                        }else {
                            success(data)
                        }
                    } else {
                        success(data)
                    }
                })
                .catch(e => {
                    console.log('----------post api hit-----------------------')
                    console.log('post url : ', url)
                    console.log('post data : ', data)
                    console.error('post parse e : ', e)
                    console.log('----------end of post api hit-----------------------')
                    handleServerError(e, errorCallback)
                })
        })
        .catch(e => {
            console.log('----------post api hit-----------------------')
            console.log('post url : ', url)
            console.log('post data : ', data)
            console.log('post raw e : ', e)
            console.log('----------end of post api hit-----------------------')
            handleServerError(e, errorCallback)
        })
}



export default Api = {
    host: host,
    serverUrl: serverUrl,
    configuration: configuration,
    generalHeaders: generalHeaders,
    get: get,
    post: post,
}