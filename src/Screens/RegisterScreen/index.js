import React from 'react'
import {
    StyleSheet,
    View,
    TextInput,
    Button,
    Text,
    Alert,
    ScrollView,
    RefreshControl,
    LayoutAnimation
} from 'react-native'
import Janus from '../../Janus'
import Api from '../../Janus/Api'
const usernameRegex = /^[a-zA-Z0-9]+$/;
const welcomeSubText = 'Please enter a username to register and proceed'
const websocket = new WebSocket('ws://10.0.13.207:8188', 'janus-protocol');
websocket.onmessage = msg => {
    console.log('sock msg',msg)
}
websocket.onopen = o => {
    console.log('sock open',o)
    websocket.send(JSON.stringify({
        janus:"create",
        transaction:Janus.randomString(12)
    }))
    
}
websocket.onerror = e => {
    console.log('sock err',e)

}
export default class RegisterScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            editable: true,
            welcomeSubText: 'Connecting to server...',
            disabled: false,
            isConnected: false,
            loading: true,
            color: '#ccc'
        }
        console.log(websocket)

    }
    componentDidMount() {
        // this.connectToServer()
    }
    setUsername = (t) => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({ username: t })
    }
    register = () => {
        if (this.state.username === '') {
            Alert.alert('Username Required', 'Please enter your username')
            return
        }
        if (!usernameRegex.test(this.state.username)) {
            Alert.alert('Invalid Username', 'Please user alphabets and numbers only')
            return
        }
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({ disabled: true, editable: false, welcomeSubText: "Please wait while we register your username" }, () => {
            Janus.registerUser({
                "request": "register",
                "username": this.state.username,
                "video": true,
                "audio": true
            }, success => {
                this.props.navigation.navigate('StartCallScreen', {
                    username: this.state.username
                })
            }, er => {
                this.setState({ disabled: true, editable: false, welcomeSubText: welcomeSubText })
            })
        })
    }
    connectToServer = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({ welcomeSubText: 'Connecting to server...', isConnected: false }, () => {
            setTimeout(() => {
                Janus.initializeJanusServer(success => {
                    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
                    this.setState({
                        welcomeSubText: 'Initializing videocall plugin',
                        isConnected: false
                    }, () => {
                        Janus.initializeJanusPlugin(s => {
                            LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
                            this.setState({
                                welcomeSubText: 'Successfully connected to server and videocall plugin initialized! ' + welcomeSubText,
                                isConnected: true,
                                loading: false,
                                editable: true,
                                disabled: false,
                            })
                        }, er => {
                            LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
                            this.setState({
                                welcomeSubText: 'Error initializing videocall plugin',
                                isConnected: 'error',
                                loading: false,
                                editable: true,
                                disabled: false,

                            })
                        })
                    })
                }, error => {
                    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
                    this.setState({
                        welcomeSubText: 'Error connecting to host ' + Api.host,
                        isConnected: 'error',
                        loading: false,
                        editable: true,
                        disabled: false,

                    })
                })
            }, 1000)

        })
    }
    renderButtons = () => {
        switch (this.state.isConnected) {
            case true:
                return (
                    <Button
                        disabled={this.state.disabled}
                        title='Register Username'
                        onPress={this.register}
                    />
                )
            case 'error':
                return (
                    <Button
                        disabled={this.state.disabled}
                        title={'Reconnect to ' + Api.host}
                        onPress={this.connectToServer}
                    />
                )
        }
    }
    changeColor = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);

        this.setState({ color: '#333' })
    }
    changeColorBack = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);

        this.setState({ color: '#ccc' })
    }
    render() {
        return (
            <ScrollView
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.loading}
                        onRefresh={this.connectToServer}
                    />}
                style={styles.mainContainer}>
                <Text style={styles.welcomeText}>
                    Welcome To Janus
                </Text>
                <Text style={[styles.welcomeSubText, { color: (this.state.isConnected === true) ? 'green' : (this.state.isConnected === 'error') ? 'red' : '#333' }]}>
                    {this.state.welcomeSubText}
                </Text>
                {this.state.isConnected === true && <TextInput
                    onBlur={this.changeColorBack}
                    onFocus={this.changeColor}
                    editable={this.state.editable}
                    autoCapitalize='none'
                    autoCompleteType='off'
                    returnKeyType={'go'}
                    autoCorrect={false}
                    spellCheck={false}
                    selectTextOnFocus={true}
                    textContentType='username'
                    onSubmitEditing={this.register}

                    onChangeText={this.setUsername}
                    placeholder='Enter Username Here'
                    style={[styles.usernameField, { borderBottomColor: this.state.color }]}
                />}
                {this.renderButtons()}
            </ScrollView>
        )
    }
}
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        padding: 10
    },
    usernameField: {
        fontSize: 20,
        color: '#333333',
        padding: 0,
        margin: 0,
        borderBottomColor: '#ccc',
        borderBottomWidth: 2,
        // borderRadius: 9,
        padding: 10,
        marginBottom: 10
    },
    welcomeText: {
        color: '#333333',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20,
        marginVertical: 10
    },
    welcomeSubText: {
        color: '#333333',
        marginBottom: 10,
        fontSize: 15,
        paddingHorizontal: 10
    }
})