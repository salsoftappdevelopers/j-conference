import React from 'react'
import { StyleSheet, View, Text, TextInput, Button, Alert, ScrollView, RefreshControl } from 'react-native'
import Janus from '../../Janus'
import Api from '../../Janus/Api'
import { NavigationEvents } from 'react-navigation'
export default class StartCallScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            loading: true
        }
        this.icomingcall = null
        this.waiting = null
    }
    componentWillUnmount() {
        // clearInterval(this.waiting)
    }
    acceptCall = (d) => {
        this.props.navigation.navigate('VideoScreen', { incomingCall: d })
    }
    rejectCall = () => {

        Janus.callHung(call => {
            console.log('call call', call)
        }, callError => {
            console.log('call call', callError)

        })
    }
    getUsers = () => {
        Janus.getRegisteredUsers(res => {
            let username = this.props.navigation.state.params.username

            if (res.plugindata) {
                let x = res.plugindata.data.result.list.findIndex(u=>u === username)
                if (x > -1) {
                    Alert.alert('Registration Successful', 'Please select one of the registered user to call or wait for someone to call')

                    this.setState({ users: res.plugindata.data.result.list, loading: false, username: username })
                } else {
                    Alert.alert('Registration failed ', 'Please register your username again')
                    this.props.navigation.pop()

                }
            }else{
                console.log(res)
                Alert.alert('asd')
            }
        }, error => {
        
            Alert.alert('Error','An error occured, please register your username')
            this.props.navigation.pop()
            console.log*(error)
            this.setState({ loading: false })
        })
    }
    componentDidMount() {
        this.waiting = '0'
        this.getUsers()

    }
    call = (username) => {
        this.props.navigation.navigate('VideoScreen', {
            username: username
        })
    }
    clearListener = () => {
        clearInterval(this.waiting)
        this.waiting = null

    }
    listenToCall = () => {
        console.log('listening')
        if (this.waiting === null) {
            return
        }
        Api.get(Janus.getSession(), d => {
            this.listenToCall()
            if (d.janus === 'event') {
                console.log(d.plugindata.data)
                if (d.plugindata.data.result.event === 'incomingcall') {
                    this.icomingcall = d
                    Alert.alert(
                        'Incoming Call',
                        d.plugindata.data.result.username + ' is calling you',
                        [
                            {
                                text: 'Accept',
                                style: 'default',
                                onPress: () => this.acceptCall(d)
                            },
                            {
                                text: 'Reject',
                                style: 'destructive',
                                onPress: this.rejectCall
                            }
                        ]
                    )
                }
            }

        }, error => {
            this.listenToCall()
            console.log(error)
        })

    }
    renderRegisteredUsers = () => {
        if (this.state.users) {
            if (Array.isArray(this.state.users)) {
                return this.state.users.map(singleUser => {
                    if (singleUser != this.state.username) {
                        return <Button
                            title={singleUser}
                            key={singleUser}
                            onPress={() => { this.call(singleUser) }}
                        />
                    }

                })
            } else {
                return <Text style={styles.welcomeSubText}>
                    Could not get getRegisteredUsers
                        </Text>
            }
        } else {
            return <Text style={styles.welcomeSubText}>
                Getting Registered Users
            </Text>
        }
    }
    renderUsername = () => {
        if (this.state.username != '') {
            return (
                <Text style={styles.welcomeText}>
                    Registered as "{this.state.username}"
                </Text>
            )
        } else {
            return (
                <Text style={styles.welcomeText}>
                    Please wait...
                </Text>
            )
        }

    }
    render() {
        return (
            <ScrollView
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.loading}
                        onRefresh={this.getUsers}
                    />}
                style={styles.mainContainer}>
                <NavigationEvents
                    onDidFocus={this.listenToCall}
                    onWillBlur={this.clearListener}
                />
                {this.renderUsername()}
                {this.renderRegisteredUsers()}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        padding: 10
    },
    usernameField: {
        fontSize: 20,
        color: '#333333',
        padding: 0,
        margin: 0,
        borderColor: '#333333',
        borderWidth: 2,
        borderRadius: 9,
        padding: 10
    },
    welcomeText: {
        color: '#333333',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20,
        marginVertical: 10
    },
    welcomeSubText: {
        color: '#333333',
        marginBottom: 10,
        fontSize: 15,
        paddingHorizontal: 10
    }
})