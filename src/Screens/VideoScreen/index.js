import React from 'react'
import {
    StyleSheet,
    View,
    Dimensions,
    Alert
} from 'react-native'
import {
    RTCPeerConnection,
    RTCSessionDescription,
    RTCView,
    mediaDevices
} from 'react-native-webrtc';
import Janus from '../../Janus'
import Api from '../../Janus/Api'

const localPc = new RTCPeerConnection(Api.configuration);

export default class VideoScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
        this.waiting = null
    }
    initLocalPC = (callback) => {
        localPc.createOffer().then(desc => {
            console.log('offer ',desc)
            let data = {
                type:desc.type,
                sdp:desc.sdp
            }
            callback(data)
          
        }).catch(e=>{
            console.log('initlocalPC e',e)
        })
    }
    getStatus = () => {
        Api.get(Api.getSession(),su=>{
            console.log('accepted ',su)
        },err=>{
            console.log('accepted err',err)
        })
    }
    componentDidMount() {

        localPc.onaddstream = stream => {
            console.log('≈', stream)
            this.setState({ remoteStream: stream.stream.toURL() })
        }

        this.getLocalMedia(streamUrl => {
            this.setState({ localStream: streamUrl })
        
            console.log(this.props.navigation)
            if (this.props.navigation.state.params.incomingCall) {
                localPc.setRemoteDescription(new RTCSessionDescription(this.props.navigation.state.params.incomingCall.jsep))
                .then(()=>{
                    localPc.createAnswer().then(des=>{
                        Janus.acceptCall({username:this.props.navigation.state.params.incomingCall.username,jsep:{sdp:des.sdp,type:des.type,}},s=>{
                          this.getStatus()
                        },e=>{
                            console.log(e)
                        })
                    })
                })
                    
        
            }else{
                this.initLocalPC(desc => {
                    localPc.setLocalDescription(desc).then(() => {

                    });
                    let callingData = {
                        jsep: desc,
                        "request": "call",
                        "username": this.props.navigation.state.params.username
                    }
                    Janus.callRegisteredUser(callingData, call => {

                        this.waiting = setInterval(() => {
                            Api.get(Janus.getSession(), d => {
                                if (d.janus === 'event') {
                                    switch (d.plugindata.data.result.event) {
                                        case 'accepted':
                                            if (d.jsep) {
                                                if (d.jsep.type === 'answer') {
                                                    localPc.setRemoteDescription(new RTCSessionDescription(d.jsep))
                                                        .then(() => {

                                                            // clearInterval(this.waiting)
                                                            // this.waiting = null;
                                                        })
                                                }
                                            }
                                            break;
                                        case 'hangup':
                                            if (this.waiting != null) {
                                                Alert.alert('Call ended', "Your session has ended ")
                                                clearInterval(this.waiting)
                                                this.waiting = null;
                                                this.props.navigation.pop()
                                            }

                                            break;
                                    }

                                }

                            }, error => {
                                console.log(error)

                            })
                        }, 1000)
                    }, callError => {
                        console.log('call ', callError)

                    })
             
                
                    
                    // localPc.setRemoteDescription(new RTCSessionDescription(this.props.navigation.state.params.incomingCall.jsep))
                    //     .then(() => {
                            
                    //         // clearInterval(this.waiting)
                    //         // this.waiting = null;
                    //     })
                

            })
            }
          
        })
    }
    getLocalMedia = (callback) => {
        mediaDevices.enumerateDevices().then(sourceInfos => {
            console.log(sourceInfos);
            let videoSourceId;
            for (let i = 0; i < sourceInfos.length; i++) {
                const sourceInfo = sourceInfos[i];
                if (sourceInfo.kind == "videoinput" && sourceInfo.facing == ("front")) {
                    videoSourceId = sourceInfo.deviceId;
                }
            }
            mediaDevices.getUserMedia({
                audio: true,
                video: {
                    mandatory: {
                        minWidth: Dimensions.get('window').width, // Provide your own width, height and frame rate here
                        minHeight: Dimensions.get('window').height,
                        minFrameRate: 30
                    },
                    facingMode: "user",
                    optional: (videoSourceId ? [{ sourceId: videoSourceId }] : [])
                }
            })
                .then(stream => {
                    localPc.addStream(stream)
                    callback(stream.toURL())
                })
                .catch(error => {
                    console.log('getUserMedia error ',error)
                });
        });
    }
    renderLocalStream = () => {
        if (this.state.localStream) {
            return (
                <RTCView key={'asdasd' + new Date().toString()} streamURL={this.state.localStream} style={{ flex: 1 }} />

            )
        }
    }
    renderRemoteStream = () => {
        if (this.state.remoteStream) {
            return (
                <RTCView key={'adfdrr' + new Date().toString()} streamURL={this.state.remoteStream} style={{ flex: 1 }} />
            )
        }
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                {this.renderLocalStream()}
                {this.renderRemoteStream()}
            </View>
        )
    }
}

const styles = StyleSheet.create({

})