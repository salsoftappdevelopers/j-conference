import { createStackNavigator } from "react-navigation-stack";
import RegisterScreen from "../../Screens/RegisterScreen";
import StartCallScreen from "../../Screens/StartCallScreen";
import { createAppContainer } from "react-navigation";
import VideoScreen from "../../Screens/VideoScreen";

const Screens = createStackNavigator({
    RegisterScreen:{
        screen:RegisterScreen,
        navigationOptions:{
            title:'Register to Janus'
        }
    },
    StartCallScreen:{
        screen:StartCallScreen,
        navigationOptions:{
            title:'Video Call'
        }

    },
    VideoScreen:{
        screen:VideoScreen,
        navigationOptions:{
            header:null
        }
    }
})

export default MainNavigator = createAppContainer(Screens)